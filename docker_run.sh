#!/bin/bash

INSTANCE_NAME=kernel-dev-debian11
IMAGE_NAME=$INSTANCE_NAME-image
HOST_NAME=$INSTANCE_NAME
HOST_USER=$USER
TARGET_USER=user
USER_CODE_DIR=/home/$HOST_USER/Documents/
TARGET_CODE_DIR=/home/$TARGET_USER/Documents/
CONTAINER_NAME="$INSTANCE_NAME-machine"

docker run -e "TERM=xterm-256color" --privileged --network host --mount type=bind,source=$USER_CODE_DIR,target=$TARGET_CODE_DIR --name=$CONTAINER_NAME -h $HOST_NAME -it $IMAGE_NAME
