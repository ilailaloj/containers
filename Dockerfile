FROM debian:11
ENV DEBIAN_FRONTEND=noninteractive

RUN apt -o "Acquire::https::Verify-Peer=false" update && apt -o "Acquire::https::Verify-Peer=false" install curl -y

# Установка пакетов
RUN apt update & apt install -y build-essential sudo sed
RUN apt update && apt upgrade -y

# создание пользователя и установка прав пользователя
RUN useradd -c 'ilia user' -m -d /home/ilia -s /bin/bash ilia 
RUN sed -i -e '/\%sudo/ c \
\%sudo ALL=(ALL) NOPASSWD: ALL' /etc/sudoers && usermod -a -G sudo ilia

# удаление ненужных пакетов
RUN apt-get autoclean && apt-get autoremove && rm -rf /var/lib/apt/lists/*

USER ilia
WORKDIR /home/ilia

RUN echo "IMAGE BUILD IS COMPLETE"
